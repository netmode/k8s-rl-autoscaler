import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
import requests
import os
import kubernetes
from kubernetes import client, config
import json
import subprocess
import pint
import time
import yaml
from pprint import pprint
import math
import datetime

unit = pint.UnitRegistry()
unit.define('core = 1 = ')
unit.define('mili_nodes = core/1000 = m')
unit.define('nano_nodes = core/1000000000 = n')
unit.define('kibi = 1 = Ki')
unit.define('mibi = 1000*kibi = Mi')

class K8sEnvWithPerformanceMetric(gym.Env):
  metadata = {'render.modes': ['human']}

  def __init__(self, app_name, cpu_request, cpu_limit, memory_request, memory_limit, sla_throughput, sla_latency,prometheus_host,prometheus_requests_metric_name,prometheus_denied_requests_metric_name,prometheus_latency_metric_name):
      # General variables defining the environment
      # Get following info from k8s
      self.hpa_baseline= '/gym_k8s/data/hpa-v2.yaml'
      self.new_hpa_baseline= '/gym_k8s/data/new-hpa-v2.yaml'
      self.MAX_PODS = 10
      self.MIN_PODS = 1
      self.cpu_request = unit.Quantity(cpu_request)
      self.memory_request = unit.Quantity(memory_request)
      self.cpu_limit =  unit.Quantity(cpu_limit)
      self.memory_limit = unit.Quantity(memory_limit)
      self.app_name = app_name
      self.sla_throughput = float(sla_throughput)
      self.sla_latency = float(sla_latency)
      self.prometheus_host = prometheus_host
      self.prometheus_requests_metric_name = prometheus_requests_metric_name
      self.prometheus_denied_requests_metric_name = prometheus_denied_requests_metric_name
      self.prometheus_latency_metric_name = prometheus_latency_metric_name
      # [pod_cpu, pod_memory, pod_numer, pods_backoff, hpa_error, pod_throughput, pod_latency ]
      self.observation_space = spaces.Box(np.array([0,0,1,0,0,0,0,0]), np.array([100,100,self.MAX_PODS,self.MAX_PODS,1,10*self.sla_throughput,10*0.99,10*self.sla_latency]))
      self.action_space = spaces.Tuple((spaces.Discrete(10),spaces.Discrete(10)))

  def step(self, action):
      '''
        Returns
        -------
        ob, reward, episode_over, info : tuple
            ob : List[int]
                an environment-specific object representing your observation of
                the environment.
            reward : float
                amount of reward achieved by the previous action. The scale
                varies between environments, but the goal is always to increase
                your total reward.
            info : Dict
                 diagnostic information useful for debugging. It can sometimes
                 be useful for learning (for example, it might contain the raw
                 probabilities behind the environment's last state change).
                 However, official evaluations of your agent are not allowed to
                 use this for learning.
      '''
      #print('cpu metric value: ' + str(action[0]))
      #print('memory metric value: ' + str(action[1]))
      # create hpa
      self._take_action(action)
      # wait 1 minute for the hpa to take effect
      time.sleep(120)
      ob = self._get_state()
      # calculate reward
      reward = self._get_reward(ob)
      now= datetime.datetime.now()
      dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
      return ob, reward, False, {'datetime':dt_string}

  def reset(self):
      return self._get_state()

  def render(self, mode='human'):
      return None

  def close(self):
      pass

  def _take_action(self,action):
      # see if there are any existing hpa
      msg1 = subprocess.getoutput('kubectl get hpa '+ self.app_name)
      print(msg1)
      v2 = client.AutoscalingV2beta2Api()
      if (action[0]==-1 and action[1]==-1):
          return
      if ('not found' not in msg1):
          #delete the hpa
          api_response = v2.delete_namespaced_horizontal_pod_autoscaler(name = self.app_name, namespace='default', pretty='true')
          #pprint(api_response)

      my_metrics = []
      if (action[0]!=0 and action[0]!=-1):
          my_metrics.append(client.V2beta2MetricSpec(type='Resource', resource= client.V2beta2ResourceMetricSource(name='cpu', target=client.V2beta2MetricTarget(average_utilization= action[0],type='Utilization'))))

      if (action[1]!=0 and action[1]!=-1):
          my_metrics.append(client.V2beta2MetricSpec(type='Resource', resource= client.V2beta2ResourceMetricSource(name='memory', target=client.V2beta2MetricTarget(average_utilization= action[1],type='Utilization'))))

      if  (len(my_metrics) >0):
          #print("my_metrics")
          #print(my_metrics)
          my_conditions = []
          my_conditions.append(client.V2beta2HorizontalPodAutoscalerCondition(status = "True", type = 'AbleToScale'))
          #print("my_conditions")
          #print(my_conditions)

          status = client.V2beta2HorizontalPodAutoscalerStatus(conditions = my_conditions, current_replicas = 1, desired_replicas = 1)

          body = client.V2beta2HorizontalPodAutoscaler(
              api_version='autoscaling/v2beta2',
              kind='HorizontalPodAutoscaler',
              metadata=client.V1ObjectMeta(name=self.app_name),
              spec= client.V2beta2HorizontalPodAutoscalerSpec(
                  max_replicas=self.MAX_PODS,
                  min_replicas=self.MIN_PODS,
                  metrics = my_metrics,
                  scale_target_ref = client.V2beta2CrossVersionObjectReference(kind = 'Deployment', name = self.app_name, api_version = 'apps/v1'),
              ),
              status = status)

          try:
              api_response = v2.create_namespaced_horizontal_pod_autoscaler(namespace='default', body=body, pretty=True)
              pprint(api_response)
          except:
              #print("Exception when calling AutoscalingV2beta1Api->create_namespaced_horizontal_pod_autoscaler" )
              print("new namespaced_horizontal_pod_autoscaler is created" )


  def _get_state(self):
      """
      Get the observation.
      pod_cpu: First number is the current pod cpu
      pod_memory: Second number is the current pod memory
      pods_number: Third number is the number of the current pods
      pods_backoff: Fourth number is the number of pods that have been killed so far due to memory_limit
      hpa_error: Fifth number informs whether there is some error with and enforced hpa (eg. wrong configuration of existing hpa)
      sla_throughput: Sixth number  refers to the average throughput by the set of deployed pods in requests/time_period. (if any. Default value is 0)
      sla_latency: Seventh number refers to the average latency by the set of deployed pods in seconds. (if any. Default value is 0)
      """
      # get metrics from metrics-server API
      p = subprocess.getoutput('kubectl get --raw /apis/metrics.k8s.io/v1beta1/namespaces/default/pods')
      #print(p)

      ret_metrics = json.loads(p)
      items = ret_metrics['items']
      #pod_cpu = 0 * unit.core
      pod_cpu = 0 * unit.core
      pod_memory = 0 * unit.mibi
      pod_number = 0
      for item in items:
          name = item['metadata']['name']
          if self.app_name in name:
              containers_array = item['containers']
              pod_number += 1
              for container in containers_array:
                  #print ("pod_cpu raw", container['usage']['cpu'])
                  #print("pod_cpu to unit core", unit.Quantity(container['usage']['cpu']).to(unit.core))
                  pod_cpu += unit.Quantity(container['usage']['cpu']).to(unit.core)
                  #print ("pod_memory raw", container['usage']['memory'])
                  #print("pod_memory to unit Mi", unit.Quantity(container['usage']['memory']).to(unit.mibi))
                  pod_memory += unit.Quantity(container['usage']['memory']).to(unit.mibi)
              #print ("total pod_cpu", pod_cpu)
              #print ("total pod_memory", pod_memory)

      # use kubernetes-client
      config.load_kube_config()
      v1 = client.CoreV1Api()
      events_list = v1.list_event_for_all_namespaces()
      events = events_list.items
      pods_backoff = 0
      for event in events:
          if (self.app_name in event.metadata.name and event.reason =='BackOff'):
              print("Event: %s %s %s %s" % ( event.metadata.name, event.type, event.reason, event.count))
              pods_backoff += int(event.count)

      # see if there are any existing hpa
      #ob_hpa =  self._get_existing_hpa()
      ob_hpa =  self._get_existing_app_hpa()
      #get latest value of performance metric
      pod_throughput = 0

      num_of_requests_metric_name = self.prometheus_requests_metric_name
      num_of_requests_response = requests.get(self.prometheus_host + '/api/v1/query', params={'query': num_of_requests_metric_name})
      results_num_of_requests = num_of_requests_response.json()['data']['result']
      for result in results_num_of_requests:
          num_of_requests = float(result['value'][1])

      pod_throughput_level = 0
      pod_throughput_rate = 1
      if (num_of_requests>0):
          throughput_level = self.prometheus_requests_metric_name +" - "+ self.prometheus_denied_requests_metric_name
          #print(throughput_level)
          throughput_rate = "("+self.prometheus_requests_metric_name +" - "+ self.prometheus_denied_requests_metric_name +") / ("+ self.prometheus_denied_requests_metric_name +")"
          #print(throughput_rate)

          throughput_level_response = requests.get(self.prometheus_host + '/api/v1/query', params={'query': throughput_level})
          throughput_rate_response = requests.get(self.prometheus_host + '/api/v1/query', params={'query': throughput_rate})

          #print(throughput_response.content)
          results_throughput_level = throughput_level_response.json()['data']['result']
          for result in results_throughput_level:
              pod_throughput_level = float(result['value'][1])

          results_rate = throughput_rate_response.json()['data']['result']
          for result in results_rate:
              pod_throughput_rate = float(result['value'][1])

      pod_latency = 1000
      latency_response = requests.get(self.prometheus_host + '/api/v1/query', params={'query': self.prometheus_latency_metric_name})
      results = latency_response.json()['data']['result']
      for result in results:
          pod_latency = float(result['value'][1])

      #ob = [pod_cpu.magnitude,pod_memory.magnitude,pod_number,pods_backoff,ob_hpa[0],ob_hpa[1],ob_hpa[2]]
      #ob = [pod_cpu/pod_number,pod_memory/pod_number,pod_number,pods_backoff,ob_hpa[0],ob_hpa[1],ob_hpa[2]]
      #print("hpa error:",ob_hpa[0],"cpu hpa theshold:",ob_hpa[1],"memory hpa theshold:",ob_hpa[2])
      #ob = [pod_cpu.to(unit.core),pod_memory.to(unit.mibi)/pod_number,pod_number,pods_backoff,ob_hpa[0],pod_throughput,pod_latency]
      ########################
      #print("pod cpu to core", pod_cpu.to(unit.core))
      #print("pod cpu_request to core", self.cpu_request.to(unit.core))
      #print("pod cpu %", 100*(pod_cpu.to(unit.core)/self.cpu_request.to(unit.core)))
      ########################
      #print("pod memory to mibi", pod_memory)
      #print("pod memory_request to mibi", self.memory_request.to(unit.mibi))
      #print("pod memory %", 100*(pod_memory.to(unit.mibi)/self.memory_request.to(unit.mibi)))
      ########################
      #ob = [pod_cpu.to(unit.core),pod_memory.to(unit.mibi)/pod_number,pod_number,pods_backoff,ob_hpa[0],pod_throughput,pod_latency]
      pod_cpu_percent = 100*(pod_cpu.to(unit.core)/self.cpu_request.to(unit.core))
      pod_memory_percent = 100*(pod_memory.to(unit.mibi)/self.memory_request.to(unit.mibi))
      ob = [pod_cpu_percent.magnitude,pod_memory_percent.magnitude,pod_number,pods_backoff,ob_hpa[0],pod_throughput_level,pod_throughput_rate,pod_latency]
      print('----CURRENT STATE---')
      print(ob)
      return ob

  def _get_reward(self, ob):
      """Reward is given depending on the
       Calculate reward value: The environment receives the current values of pod_number and cpu/memory metric values
       that correspond to the current state of the system s. The reward value r is calculated based on two criteria:
       (i) the amount of resources acquired, which directly determine the cost, and
       (ii) the number of pods needed to support the received load.
      """
      reward_max = 100
      reward_min = 0
      reward = 0
      pod_number = ob[2] # number of pods
      pod_throughput_level = float(ob[5]) # pod throughput
      pod_throughput_rate = float(ob[6]) # pod throughput
      pod_latency = float(ob[7]) # pod latency
      pods_backoff = ob[3] # number backed off pods
      hpa_error = ob[4] #whether the existing hpa is succesfully enforced (if any. Default value is 0)
      d = float(5) # this is a hyperparamter of the reward function

      if pods_backoff > 0 :  #to be defined: watch on terminated pods
          print('pods backoff: ', pods_backoff)
          reward = -100 * pods_backoff
          return reward
      if hpa_error == 1 : #whether there is some error with and enforced hpa (eg. wrong configuration of existing hpa)
           print('hpa has failed to get enforced')
           reward= -100 * (self.MAX_PODS+1)
           return reward

      if (pod_number ==1 and pod_throughput_level <= (1.05 * self.sla_throughput) and pod_throughput_rate >= 0.99 and pod_latency<= self.sla_latency):
          reward = reward_max
          return reward
      else :
          reward = -100/(self.MAX_PODS-1) * pod_number + 100*self.MAX_PODS/(self.MAX_PODS-1)


      ####throughput
      if  (pod_throughput_rate  >= 0.99 and  pod_throughput_level < 1.05 * self.sla_throughput):
          reward += 100* pow(math.e, -d*pow(1-pod_throughput_level/self.sla_throughput,2))
      else:
          reward += 100* pow(math.e, -10*d*pow(1-pod_throughput_level/self.sla_throughput,2))

      ####latency
      if  (pod_latency < 0.95 * self.sla_latency):
          reward += 100* pow(math.e, -d*pow(1-pod_latency/self.sla_latency,2))
      else:
          reward += 100* pow(math.e, -10*d*pow(1-pod_latency/self.sla_latency,2))

      reward = reward/3

      return reward

  def _get_existing_app_hpa(self):
      hpa_error = 0
      pod_cpu_threshold = 0
      pod_memory_threshold = 0
      # see if there are any existing hpa
      v2 = client.AutoscalingV2beta2Api()
      name = self.app_name # str | name of the HorizontalPodAutoscaler
      namespace = 'default' # str | object name and auth scope, such as for teams and projects
      pretty = 'true'

      try:
          ret = v2.read_namespaced_horizontal_pod_autoscaler(name, namespace, pretty=pretty)
          #pprint(ret)
          if (item.code):
              return [hpa_error,pod_cpu_threshold,pod_memory_threshold]
          else:
              if(item.metadata.name == self.app_name) :
                  print("%s\t%s\t%s" % (item.spec.max_replicas, item.metadata.namespace, item.metadata.name))
                  for condition in item.status.conditions:
                      print("%s\t%s\t%s\t%s" % (condition.status, condition.reason, condition.type, condition.message))
                      if condition.reason !='DesiredWithinRange' and  condition.status == str(False):
                          hpa_error = 1
                          return [hpa_error,pod_cpu_threshold,pod_memory_threshold]
                      metrics = item.spec.metrics
                      for metric in metrics:
                          if metric.resource.name == 'cpu':
                              pod_cpu_threshold =  metric.resource.target.average_utilization
                          if metric.resource.name == 'memory':
                              pod_memory_threshold =  metric.resource.target.average_utilization
          return [hpa_error,pod_cpu_threshold,pod_memory_threshold]
      except:
          #print("Exception when calling AutoscalingV2beta2Api->read_namespaced_horizontal_pod_autoscaler:")
          return [hpa_error,pod_cpu_threshold,pod_memory_threshold]

METRIC_LIST = {
    'none' : 'none',
    'cpu' : 'cpu-percent',
    'memory' : 'memory-percent'
}

VALUE_LIST = {0,20,40,60,80,100}
