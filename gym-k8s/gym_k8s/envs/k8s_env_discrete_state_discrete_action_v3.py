import datetime
import math
import time
from pprint import pprint

import numpy as np
import requests
from gym import spaces
from gym.envs.toy_text import discrete
from kubernetes import client, config

twenty = 0
forty = 1
sixty = 2
eighty = 3
hundred = 4
hundred_fifty = 5
two_hundred = 6


class K8sEnvDiscreteStateDiscreteActionV3(discrete.DiscreteEnv):
    metadata = {
        'render.modes': ['human']
    }

    def __init__(self, app_name, sla_latency, prometheus_host, prometheus_latency_metric_name):
        config.load_kube_config()

        # General variables defining the environment
        # Get following info from k8s
        num_states = 175
        num_actions = 3
        P = {
            state: {
                action: [] for action in range(num_actions)
            } for state in range(num_states)
        }
        initial_state_distrib = np.zeros(num_states)
        self.done = False
        self.MAX_PODS = 10
        self.MIN_PODS = 1
        self.app_name = app_name
        self.sla_latency = float(sla_latency)
        self.prometheus_host = prometheus_host
        self.prometheus_latency_metric_name = prometheus_latency_metric_name

        self.observation_space = spaces.Tuple((
            spaces.Discrete(5),  # pod_cpu_percent
            spaces.Discrete(5),  # pod_replicas_percent
            spaces.Discrete(7)   # latency_percent
        ))

        self.action_space = spaces.Discrete(3)
        discrete.DiscreteEnv.__init__(
            self, num_states, num_actions, P, initial_state_distrib
        )

    def step(self, action):
        """
        Returns
        -------
        encoded_observation, reward, done, dt_dict : tuple
            encoded_observation : int
                discretized environment observation encoded in an integer
            reward : float
                amount of reward achieved by the previous action. The scale
                varies between environments, but the goal is always to increase
                your total reward.
            done : boolean
                boolean value of whether training is done or not. Becomes True
                when errors occur.
            dt_dict : Dict
                dictionary of formatted date and time.
        """
        self._take_action(action)  # Create HPA
        time.sleep(480)  # Wait 8 minutes for the changes to take place

        encoded_observation, real_observation = self._get_state()
        reward = self._get_reward(real_observation)

        if -1 in self.decode(encoded_observation):
            self.done = True
            reward = 0

        now = datetime.datetime.now()
        dt_string = now.strftime('%d/%m/%Y %H:%M:%S')
        dt_dict = {
            'datetime': dt_string
        }

        return encoded_observation, reward, self.done, dt_dict

    def reset(self):
        return self._get_state()

    def render(self, mode='human'):
        return None

    def close(self):
        pass

    def _take_action(self, action):
        if action == 1:
            return

        (hpa_error,
         pod_cpu_current_util,
         pod_cpu_threshold,
         current_replicas) = self._get_existing_app_hpa()

        if hpa_error == 1:
            self.done = True
        else:
            self.done = False

        v2 = client.AutoscalingV2beta2Api()
        if pod_cpu_threshold != 0:
            # Delete the hpa
            api_response = v2.delete_namespaced_horizontal_pod_autoscaler(
                name=self.app_name, namespace='default', pretty='true'
            )

        # Adjust the threshold
        new_cpu_hpa_threshold = pod_cpu_threshold

        if action == 0 and pod_cpu_threshold > 10:
            new_cpu_hpa_threshold -= 10

        if action == 2 and pod_cpu_threshold < 100:
            new_cpu_hpa_threshold += 10

        my_metrics = []
        if new_cpu_hpa_threshold != 0:
            my_metrics.append(
                client.V2beta2MetricSpec(
                    type='Resource', resource=client.V2beta2ResourceMetricSource(
                        name='cpu', target=client.V2beta2MetricTarget(
                            average_utilization=new_cpu_hpa_threshold, type='Utilization')
                    )
                )
            )

        if len(my_metrics) > 0:
            my_conditions = []
            my_conditions.append(client.V2beta2HorizontalPodAutoscalerCondition(
                status='True', type='AbleToScale')
            )

            status = client.V2beta2HorizontalPodAutoscalerStatus(
                conditions=my_conditions, current_replicas=1, desired_replicas=1
            )

            body = client.V2beta2HorizontalPodAutoscaler(
                api_version='autoscaling/v2beta2',
                kind='HorizontalPodAutoscaler',
                metadata=client.V1ObjectMeta(name=self.app_name),
                spec=client.V2beta2HorizontalPodAutoscalerSpec(
                    max_replicas=self.MAX_PODS,
                    min_replicas=self.MIN_PODS,
                    metrics=my_metrics,
                    scale_target_ref=client.V2beta2CrossVersionObjectReference(
                        kind='Deployment', name=self.app_name, api_version='apps/v1'
                    ),
                ),
                status=status
            )

            # Create new HPA with updated thresholds
            try:
                api_response = v2.create_namespaced_horizontal_pod_autoscaler(
                    namespace='default', body=body, pretty=True
                )
                pprint(api_response)
            except Exception:
              print('Created new namespaced_horizontal_pod_autoscaler')

    def _get_state(self):
        # Get metrics from metrics-server API
        (hpa_error,
         pod_cpu_current_util,
         pod_cpu_threshold,
         current_replicas) = self._get_existing_app_hpa()

        pod_latency = 1000

        # Fetch latency through prometheus
        prometheus_endpoint = '{}/api/v1/query'.format(self.prometheus_host)
        latency_response = requests.get(
            prometheus_endpoint, params={
                'query': self.prometheus_latency_metric_name
            }
        )

        results = latency_response.json()['data']['result']
        for result in results:
            pod_latency = float(result['value'][1])

        if math.isnan(pod_latency):
            pod_latency = 1000

        current_replicas_percent = 100 * current_replicas / self.MAX_PODS
        pod_latency_percent = 100 * pod_latency / self.sla_latency

        real_observation = [
            pod_cpu_current_util,
            current_replicas_percent,
            pod_latency_percent
        ]

        discretized_observation = [self._get_discrete(ob) for ob in real_observation]
        encoded_observation = self.encode(
            discretized_observation[0],
            discretized_observation[1],
            discretized_observation[2]
        )

        return encoded_observation, real_observation

    def _get_reward(self, real_observation):
        """
        Calculate reward value: The environment receives the current values of
        pod_number and cpu/memory metric values that correspond to the current
        state of the system s. The reward value r is calculated based on two
        criteria:
        (i)  the amount of resources acquired,
             which directly determines the cost
        (ii) the number of pods needed to support the received load.
        """

        (pod_cpu_current_util,
         current_replicas_percent,
         pod_latency_percent) = real_observation

        reward_max = 100
        reward = 0

        pod_number = round(current_replicas_percent * self.MAX_PODS / 100)
        pod_latency = pod_latency_percent * self.sla_latency / 100
        d = 5.0  # this is a hyperparamter of the reward function

        latency_ratio = pod_latency / self.sla_latency
        if pod_number == 1 and latency_ratio <= 1:
            reward = reward_max
            return reward
        else:
            reward = -100 / (self.MAX_PODS - 1) * pod_number \
                + 100 * self.MAX_PODS / (self.MAX_PODS - 1)

        latency_ref_value = 0.95
        if latency_ratio < latency_ref_value:
            reward += 100 * pow(math.e, -d * pow(latency_ref_value - latency_ratio, 2))
        else:
            reward += 100 * pow(math.e, -10 * d * pow(latency_ref_value - latency_ratio, 2))

        reward /= 2

        return reward

    def _get_discrete(self, number):
        """
        Get a number and return the discrete level it belongs to
        """
        number = round(number, 0)

        if number in range(-1, 20):
            return twenty
        elif number in range(20, 40):
            return forty
        elif number in range(40,60):
            return sixty
        elif number in range(60, 80):
            return eighty
        elif number in range(80, 101):
            return hundred
        elif number in range(101, 150):
            return hundred_fifty
        elif number in range(150, 200) or number > 200:
            return two_hundred
        else:
            return -1

    def _get_existing_app_hpa(self):
        hpa_error = 0
        pod_cpu_current_util = 0
        pod_cpu_threshold = 0
        current_replicas = self.MAX_PODS

        # See if there are any existing HPA
        v2 = client.AutoscalingV2beta2Api()
        name = self.app_name
        namespace = 'default'
        pretty = 'true'

        try:
            item = v2.read_namespaced_horizontal_pod_autoscaler(
                name, namespace, pretty=pretty
            )
            if item.metadata.name == self.app_name:
                for metric in item.status.current_metrics:
                    if metric.resource.name == 'cpu':
                        pod_cpu_current_util = metric.resource.current.average_utilization

                for condition in item.status.conditions:
                    if condition.reason != 'DesiredWithinRange' and condition.status == 'False':
                        hpa_error = 1
                        return [hpa_error, pod_cpu_current_util, pod_cpu_threshold, current_replicas]

                metrics = item.spec.metrics
                for metric in metrics:
                    if metric.resource.name == 'cpu':
                        pod_cpu_threshold = metric.resource.target.average_utilization

                current_replicas = item.status.current_replicas
                return [hpa_error, pod_cpu_current_util, pod_cpu_threshold, current_replicas]
        except Exception:
            return [hpa_error, pod_cpu_current_util, pod_cpu_threshold, current_replicas]

    def encode(self, cpu, pods, latency):
        """
        Encode the discrete observation values in one single number.
        CPU and pod utilization can take values in {0, 1, 2, 3, 4}
        Latency can take values in {0, 1, 2, 3, 4, 5, 6}
        """
        assert 0 <= cpu < 5
        assert 0 <= pods < 5

        i = cpu
        i *= 5
        i += pods
        i *= 7
        i += latency
        return i

    def decode(self, i):
        out = []
        out.append(i % 7)
        i //= 7
        out.append(i % 5)
        i //= 5
        out.append(i)
        return reversed(out)
