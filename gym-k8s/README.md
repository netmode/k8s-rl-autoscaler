## k8s RL autoscaler
k8s RL autoscaler is a project that offers a set of RL environments and a set of RL agents.
The RL environments can be found at the gym-k8s package and represent the resources status and activities of a k8s cluster for a specific app.
As a PoC application php-apache is selected.
For a detailed view of each environment you can visit the separate documentation.
A set of RL agents will be implemented per environment trying to solve optimally the creation of new hpas. The high objective of the developed agents is to use as much as possible fewer pod replicas and pick up hpa thresholds that makes use satisfy the best operation of the deployed application.

## Ιnstall requirementes

`pip3 install -e gym-k8s`

## Ηow to execute an environment

```
python3
import gym
env = gym.make('gym_k8s:k8s-v0')
observation = env.reset()
env.step([-1,-1]) # no action is taken
env.step([0,0]) # any existing hpa is deleted
env.step([40,90]) # new hpa is created/replaced with cpu hpa threshold is 40% and memory hpa threshold is 90%
```


