#!/usr/bin/env bash

if [ "$#" -ne 1 ]
then
  echo "Usage: $0 <epoch_number>" >&2
  exit 1
fi

# Get the directory of this script
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

# Stress our application based on the targets defined in the 'targets.txt' file
# and using the data inside the 'body.txt' file. The results are also stored in
# the 'results.bin' file so that we can call 'vegeta report' later.
# 
# Example 'targets.txt' file:
# -----------------------------
#   POST http://localhost:8000/
#   Host: fibonacci.com
# -----------------------------

vegeta attack -targets=$SCRIPT_DIR/targets.txt -duration=32m -rate=10/1s -body=$SCRIPT_DIR/body.txt \
  | tee $SCRIPT_DIR/results-epoch-$1-phase-1.bin \
  | vegeta report

vegeta attack -targets=$SCRIPT_DIR/targets.txt -duration=32m -rate=1/1s -body=$SCRIPT_DIR/body.txt \
  | tee $SCRIPT_DIR/results-epoch-$1-phase-2.bin \
  | vegeta report
