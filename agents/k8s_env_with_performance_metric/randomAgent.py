import gym
import csv
env = gym.make('gym_k8s:k8s-with-performance-metric-v0', app_name='hello-python',cpu_request='128m',cpu_limit = '80m',memory_request = '128Mi',memory_limit = '80Mi',sla_throughput = 50, sla_latency = 1, prometheus_host = 'http://localhost:9091/', prometheus_requests_metric_name = "sum(rate(haproxy_backend_http_requests_total[20s]))",
prometheus_denied_requests_metric_name  = "sum(rate(haproxy_backend_requests_denied_total[20s]))", prometheus_latency_metric_name = "avg(haproxy_backend_response_time_average_seconds{proxy='default_hello-python_5000'})")
# Uncomment following line to save video of our Agent interacting in this environment
# This can be used for debugging and studying how our agent is performing
# env = gym.wrappers.Monitor(env, './video/', force = True)
t = 0

while True:
   t += 1
   env.render()
   observation = env.reset()
   print(observation)
   action = env.action_space.sample()
   hpa_theasholds = tuple([10*x for x in action])
   #observation, reward, done, info = env.step(hpa_theasholds)
   mytuple = env.step(hpa_theasholds)
   observation, reward, done, info = mytuple
   with open('k8s_historical_states.csv', 'a') as f:
        f.write(','.join(str(s) for s in mytuple))
        f.write(',')
        f.write(','.join(str(s) for s in hpa_theasholds))
        f.write('\n')
        f.close()
   if done:
        print("Episode finished after {} timesteps".format(t+1))
        break
env.close()
