#!/usr/bin/env bash

read -p "Deleting output/intermediate files. Continue (y/n)?" choice
case "$choice" in 
  y|Y ) rm -rf *.csv train_dir;;
  n|N ) echo "Aborted";;
  * ) echo "Invalid input";;
esac
