#!/usr/bin/env bash

if [ "$#" -ne 4 ]
then
  echo "Usage: $0 <endpoint> <rate> <timestep_duration> <timestep>" >&2
  exit 1
fi

# Get the directory of this script
script_dir=$(dirname "$(readlink -f "$0")")

endpoint=$1
rate=$2
timestep_duration=$3
timestep=$4

rate_7=$(( 7 * $rate ))
rates=($rate $rate_7 $rate_7 $rate)

current_rate=${rates[$timestep]}
echo "POST http://${endpoint}/" \
  | vegeta attack -duration=${timestep_duration}m -rate=${current_rate}/1m -body=${script_dir}/body.txt \
  | vegeta report
