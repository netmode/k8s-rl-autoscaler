import json
import numpy as np
import pandas as pd

steps_per_epoch = 16
possible_thresholds = list(range(20,100 + 1, 20))
possible_pods = list(range(1, 10 + 1))

# The tests were performed with 5 different stress phases and 16 steps per epoch
# The stress pattern is: λ->λ->3λ->3λ->5λ->5λ->7λ->7λ->9λ->9λ->7λ->7λ->5λ->5λ->3λ->3λ
# where λ is the basic rate
base_rate = 0.5
stress_timesteps_dict = {
    base_rate: {0, 1},
    3 * base_rate: {2, 3, 14, 15},
    5 * base_rate: {4, 5, 12, 13},
    7 * base_rate: {6, 7, 10, 11},
    9 * base_rate: {8, 9}
}

# Calculate the mean and standard deviation of a column of data
# by repeatedly sampling the data to create a sampling distribution
def calc_mean_and_std(data):
    if len(data) == 0:
        return None, None

    sample_size = 50
    number_of_samples = 100
    mean_array = []
    for _ in range(number_of_samples):
        samples = np.random.choice(data, sample_size)
        mean_array.append(np.mean(samples))
    return np.mean(mean_array), np.std(mean_array)

data = pd.read_csv('k8s_historical_states_discrete.csv')
distribution_dict = {
    phase: {
        'thresh': {
            thresh: {}
            for thresh in possible_thresholds
        },
        'pod': {
            pod: {}
            for pod in possible_pods
        }
    } for phase in stress_timesteps_dict
}

for phase in stress_timesteps_dict:
    current_data = data[data.timestep.mod(steps_per_epoch).isin(stress_timesteps_dict[phase])]

    for thresh in possible_thresholds:
        subset = current_data[current_data.hpa_threshold == thresh]

        replicas_data = subset.number_of_pods.to_numpy()
        replicas_mean, replicas_std = calc_mean_and_std(replicas_data)

        distribution_dict[phase]['thresh'][thresh] = {
            'replicas_mean': replicas_mean,
            'replicas_std': replicas_std
        }

    for pod in possible_pods:
        subset = current_data[current_data.number_of_pods == pod]

        cpu_util_data = subset.cpu_util.to_numpy()
        cpu_util_mean, cpu_util_mean_std = calc_mean_and_std(cpu_util_data)

        latency_data = subset.latency.to_numpy()
        latency_mean, latency_mean_std = calc_mean_and_std(latency_data)

        distribution_dict[phase]['pod'][pod] = {
            'cpu_util_mean': cpu_util_mean,
            'cpu_util_std': cpu_util_mean_std,
            'latency_mean': latency_mean,
            'latency_std': latency_mean_std
        }

with open('distributions.json', 'w') as f:
    json.dump(distribution_dict, f)
