#!/usr/bin/env bash

if [ "$#" -ne 3 ]
then
  echo "Usage: $0 <epoch_number> <rate> <timestep_duration>" >&2
  exit 1
fi

# Get the directory of this script
script_dir=$(dirname "$(readlink -f "$0")")

# Stress our application based on the targets defined in the 'targets.txt' file
# and using the data inside the 'body.txt' file. The results are also stored in
# the 'results.bin' file so that we can call 'vegeta report' later.
# 
# Example 'targets.txt' file:
# -----------------------------
#   POST http://localhost:8000/
#   Host: fibonacci.com
# -----------------------------

epoch=$1
rate=$2
timestep_duration=$3

rate_3=$(( 3 * $rate ))
rate_5=$(( 5 * $rate ))
rate_7=$(( 7 * $rate ))
rate_9=$(( 9 * $rate ))
rates=($rate $rate_3 $rate_5 $rate_7 $rate_9 $rate_7 $rate_5 $rate_3)

for index in ${!rates[@]}; do
  current_rate=${rates[$index]}
  vegeta attack -targets=$script_dir/targets.txt -duration=${timestep_duration}m -rate=$current_rate/1m -body=$script_dir/body.txt \
    | tee $script_dir/results-epoch-$epoch-phase-$index.bin \
    | vegeta report
done
