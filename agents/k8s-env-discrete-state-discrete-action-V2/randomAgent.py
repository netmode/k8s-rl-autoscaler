import gym
import csv
env = gym.make('gym_k8s:k8s-env-discrete-state-discrete-action-v2', app_name='hello-python', sla_latency = 0.2, prometheus_host = 'http://localhost:9091/',  prometheus_latency_metric_name = "avg(haproxy_backend_response_time_average_seconds{proxy='default_hello-python_5000'})")
t = 0

while True:
   t += 1
   env.render()
   observation = env.reset()
   print(observation)
   action = env.action_space.sample()
   mytuple = env.step(action)
   observation, reward, done, info = mytuple

   with open('k8s_historical_states_discrete.csv', 'a') as f:
        f.write(','.join(str(s) for s in mytuple)+','+str(action))
        f.write('\n')
        f.close()

   if done:
        print("Episode finished after {} timesteps".format(t+1))
        break
env.close()
