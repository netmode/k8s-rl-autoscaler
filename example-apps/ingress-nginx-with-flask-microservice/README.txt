How to deploy an app via an nginx ingress controller
------------------------------------------------------

##STEP 1: create hello-python microservice [1]

create a manifest with name deployment-hello-python.yaml.
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-python
spec:
  selector:
    matchLabels:
      app: hello-python
  replicas: 1
  template:
    metadata:
      labels:
        app: hello-python
    spec:
      containers:
      - name: hello-python
        image: netmode/hello-python:1.0.0
        resources:
          limits:
            memory: "128Mi"
            cpu: "80m"
          requests:
            memory: "128Mi"
            cpu: "80m"
        ports:
        - containerPort: 5000
        
---        
apiVersion: v1
kind: Service
metadata:
  name: hello-python
spec:
  selector:
    app: hello-python
  ports:
  - protocol: "TCP"
    port: 5000
  type: NodePort
```
And deploy it:

```
kubectl apply -f deployment-hello-python.yaml
```

##STEP 2: install ingress-nginx [2][3]

# ref: https://github.com/kubernetes/ingress-nginx (repo)
# ref: https://github.com/kubernetes/ingress-nginx/tree/master/charts/ingress-nginx (chart)

# 1. Create namespace
kubectl create namespace ingress-nginx

# 2. Add the repository
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

# 3. Update the repo
helm repo update

# 4. Install nginx-ingress through Helm
helm install ingress-controller ingress-nginx/ingress-nginx --namespace ingress-nginx

Bare-metal
Using NodePort:
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.43.0/deploy/static/provider/baremetal/deploy.yaml

##STEP 3: expose prometheus metrics [4]

# 1. Setup prometheus

kubectl apply --kustomize github.com/kubernetes/ingress-nginx/deploy/prometheus/

# 1. Reconfigure nging
helm upgrade ingress-controller ingress-nginx/ingress-nginx \
  --namespace ingress-nginx \
  --set controller.metrics.enabled=true \
  --set-string controller.podAnnotations."prometheus\.io/scrape"="true" \
  --set-string controller.podAnnotations."prometheus\.io/port"="10254"
  
Open the prometheus port shown through kubectl get svc -A) and start typing ng it shows the metrics.

##STEP 4: create ingress for hello-python Find ingress manifest at path and deploy it:  

create a manifest with name ingress-example.yaml

```
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ingress-node-sample-helloworld
  annotations:
    # Target URI where the traffic must be redirected
    # More info: https://github.com/kubernetes/ingress-nginx/blob/master/docs/examples/rewrite/README.md
    nginx.ingress.kubernetes.io/rewrite-target: /
    kubernetes.io/ingress.class: nginx
spec:
  rules:
    - http:
        paths:
        - path: /
          backend:
            serviceName: hello-python
            servicePort: 5000
```  
And deploy it:  

```
kubectl apply -f ingress-example.yaml
```

##STEP 5: create hpa for hello-python app

kubectl autoscale deployment hello-python --cpu-percent=80 --min=1 --max=3

##STEP 6: Stress hello-python

Find the ingress port of the service ingress-controller-ingress-nginx-controller : kubectl get svc -A

Then stress the hello-python application via the ingress controller with the use of a load generator:

`kubectl run -i --tty infinite-calls --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://<HOST>:<PORT>/hello-python; done"`

Finally you can delete the deployment so as to stop the stress process.
`kubectl delete -n default deployment infinite-calls`

References

[1] https://kubernetes.io/blog/2019/07/23/get-started-with-kubernetes-using-python/ 
[2] https://kubernetes.github.io/ingress-nginx/deploy/#bare-metal
[3] https://dev.to/xaviergeerinck/creating-a-kubernetes-nginx-ingress-controller-and-create-a-rule-to-a-sample-application-4and
[4] https://dev.to/xaviergeerinck/monitoring-the-kubernetes-nginx-ingress-controller-with-prometheus-and-grafana-35gi
