
1) Add the HAProxy Ingress’ Helm repository

https://haproxy-ingress.github.io/docs/getting-started/



2) Install HAProxy Ingress 

helm install haproxy-ingress haproxy-ingress/haproxy-ingress   --create-namespace --namespace=ingress-controller   --set controller.hostNetwork=true   --set controller.kind=DaemonSet   --set controller.service.type=NodePort


3) Deploy and expose a service
echoserver


4) create ingress
/home/eleni/PythonProjects/k8s-RL-autoscaler/example-apps/haproxy-with-flask/echoserver-ingress.yaml

https://echoserver.147.102.13.78.nip.io/memory-intensive

4) create hpa

kubectl autoscale deployment echoserver --cpu-percent=20 --min=1 --max=3

5) stress echoserver

kubectl run -i --tty infinite-calls --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://echoserver.147.102.13.78.nip.io; done"

Finally you can delete the deployment so as to stop the stress process.
kubectl delete -n default pod infinite-calls
